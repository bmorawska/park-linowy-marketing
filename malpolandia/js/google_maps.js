// Regular map https://www.youtube.com/watch?v=SRv0Ox-Hg1A
function regular_map() {
  var var_location = new google.maps.LatLng(51.748389, 19.45183);

  var var_mapoptions = {
    center: var_location,
    zoom: 14
  };

  var var_map = new google.maps.Map(
    document.getElementById("map-container"),
    var_mapoptions
  );

  var var_marker = new google.maps.Marker({
    position: var_location,
    map: var_map,
    title: "Park Klepacza"
  });
}

// Initialize maps
google.maps.event.addDomListener(window, "load", regular_map);
