//Open Modal (Image Viewer or Slide Show)
function openModal() {
  // Show Image Viewer or modal when any image is clcked
  document.getElementById("my-modal").style.display = "inline-flex";

  document.body.style.overflow = "hidden";
  document.getElementById("my-nav").style.display = "none";
}

//Close Modal (Image VIewer)
function closeModal() {
  document.getElementById("my-modal").style.display = "none";

  document.body.style.overflow = "auto";
  document.getElementById("my-nav").style.display = "flex";
}

var slideIndexJS = 1;
showSlides(slideIndexJS);

function changeSlides(n) {
  showSlides((slideIndexJS += n));
}

//Display an image  whose thumbnail is clicked
function currentSlide(n) {
  showSlides((slideIndexJS = n));
}

//Main function to operate the slide show or modal or image viewer
function showSlides(n) {
  var i;

  //Get all elements with class "my-slides"
  var slidesJS = document.getElementsByClassName("my-slides");

  /*If the current slide index is grater yhan total number of slides then open
  slideIndexJS = 1 (begin with first slide after the last slide ends) */

  if (n > slidesJS.length) {
    slideIndexJS = 1;
  }

  /* If the current slide index is less than 1, then open with slideIndexJS = 
  last (if previous button is clicked on first slide then open last slide) */

  if (n < 1) {
    slideIndexJS = slidesJS.length;
  }

  //Hide all slides initially
  for (i = 0; i < slidesJS.length; i++) {
    slidesJS[i].style.display = "none";
  }

  //Show slide which is clicked
  slidesJS[slideIndexJS - 1].style.display = "block";
}
